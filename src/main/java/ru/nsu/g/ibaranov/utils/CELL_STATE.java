package ru.nsu.g.ibaranov.utils;

public enum  CELL_STATE {
    VOID,
    APPLE,
    SNAKE;

    boolean isVoid(int index)
    {
        return index == 0;
    }
    boolean isApple(int index)
    {
        return index == 1;
    }
    boolean isSnake(int index)
    {
        return index == 2;
    }

}