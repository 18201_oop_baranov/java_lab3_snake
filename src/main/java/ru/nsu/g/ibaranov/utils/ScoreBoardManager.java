package ru.nsu.g.ibaranov.utils;

import ru.nsu.g.ibaranov.model.Model;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScoreBoardManager {

   // GameWindow gameWindow;
    Model model;

    private String convertDifficulty2(int difficulty) {
        if (difficulty == 1) return "Easy";
        if (difficulty == 2) return "Medium";
        if (difficulty == 3) return "Hard";
        if (difficulty == 4) return "Console";
        return "Medium";
    }


    public ScoreBoardManager()
    {
        this.model = new Model(1,1,1);
    }

    public ScoreBoardManager(Model model)
    {
        this.model = model;
    }

    public void printEntryList() throws IOException {
        List<Entry> entryList = loadData();

        entryList.sort((a, b) -> b.getValue() - a.getValue());

        String stringlist = "";

        for (int i=0; i<entryList.size(); i++){
            stringlist += (i+1 + ": " + entryList.get(i).toString() + "\n");
        }

        System.out.println(stringlist);
    }

    public List<Entry> loadData() throws IOException {
        Path path = Paths.get("./Statictics.txt");

        File file = new File(path.toString());
        List<Entry> list = new ArrayList<>();

        if (!file.exists()) {
            file.createNewFile();
            String emptyString = "";
            for (int i=0; i<8; i++)
            {
                Entry entry = new Entry("-", "-", 0);
                list.add(entry);
                emptyString += entry.toString() + "\n";
            }

            FileWriter fileWriter = new FileWriter(path.toString(), false);

            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write(emptyString);
            writer.close();
            return list;
        }

        String[] data;
        try (FileInputStream fis = new FileInputStream("Statictics.txt")) {
            Scanner sc = new Scanner(fis);
            while (sc.hasNextLine()) {
                data = sc.nextLine().split(" ");
                Entry entry;
                if (data.length == 3) entry = new Entry(data[0], data[1], Integer.parseInt(data[2]));
                else entry = new Entry("-", "-", 0);
                list.add(entry);
            }
        }
        catch (IOException ignored)
        {

        }
        return list;
    }

    public void saveData() throws IOException {

        List<Entry> entryList = loadData();

        entryList.add(new Entry(this.model.getCurrentPlayerName(),
                convertDifficulty2(this.model.getDifficulty()),
                this.model.getApplesEaten()));

        entryList.sort((a, b) -> b.getValue() - a.getValue());

        while (entryList.size() > 8) entryList.remove(entryList.size()-1);

        String stringlist = "";

        for (Entry entry : entryList) {
            stringlist += (entry.toString() + "\n");
        }

        Path path = Paths.get("./Statictics.txt");
        try {
            FileWriter fileWriter = new FileWriter(path.toString(), false);

            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write(stringlist);
            writer.close();
        } catch (IOException ignored) {
        }
    }
}
