package ru.nsu.g.ibaranov.utils;

public class Entry {
    private String name;
    private String difficulty;
    private int value;

    public String getDifficulty() {
        return difficulty;
    }

    public String getName()
    {
        return name;
    }

    public int getValue()
    {
        return value;
    }

    public Entry(String name, String difficulty, int value) {
        this.name = name;
        this.difficulty = difficulty;
        this.value = value;
    }

    private String convertDifficulty(int difficulty) {
        if (difficulty == 1) return "Easy";
        if (difficulty == 2) return "Medium";
        if (difficulty == 3) return "Hard";
        return "Medium";
    }

    public  String forScoreBoard()
    {
        return "      "  +  name + "                " + difficulty + "                     " + value;
    }

    @Override
    public String toString() {
        return name + " " + difficulty + " " + value;
    }
}
