package ru.nsu.g.ibaranov.utils;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT;
}

