package ru.nsu.g.ibaranov;

import ru.nsu.g.ibaranov.view.gui.GameWindow;

import java.io.IOException;

public class MainGUI {

    public static void main(String[] args) throws IOException {
        GameWindow gw = new GameWindow();
        gw.run();
    }
}
