package ru.nsu.g.ibaranov.model;


import ru.nsu.g.ibaranov.utils.Direction;
import ru.nsu.g.ibaranov.utils.Updater;

import javax.swing.Timer;
import java.awt.*;
import java.util.*;

public class Model implements Runnable {

    private int GROWTH = 1;
    private Direction direction = Direction.UP;
    private int difficulty;
    private int applesEaten;

    public int getApplesEaten() {
        return applesEaten;
    }

    private int MAX_INDEX_X;
    private int MAX_INDEX_Y;

    public Point getApple() {
        generateApple();
        return apple;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public int getMAX_INDEX_X() {
        return MAX_INDEX_X;
    }

    public int getMAX_INDEX_Y() {
        return MAX_INDEX_Y;
    }

    private Updater updater;
    public boolean isGameOver = false;
    private final Point apple = new Point();
    private final Random random = new Random();
    private String currentPlayerName = "Player";
    private ArrayDeque<Point> snakeBody = new ArrayDeque<>();
    private final LinkedHashSet<Point> occupiedPositions = new LinkedHashSet<>();
    private Timer timer;

    public void setCurrentPlayerName(String currentPlayerName) {
        this.currentPlayerName = currentPlayerName;
    }

    public String getCurrentPlayerName() {
        return currentPlayerName;
    }

    public Deque<Point> getSnakeBody() {
        generateSnakeAtCenter();
        return snakeBody;
    }

    public void setIsGameOver(boolean value)
    {
        this.isGameOver = value;
    }

    private int squaresToGrow;
    private int scale;

    public Model(int difficulty, int MAX_INDEX_X, int MAX_INDEX_Y) {
        this.applesEaten = 0;
        this.MAX_INDEX_X = MAX_INDEX_X;
        this.MAX_INDEX_Y = MAX_INDEX_Y;
        this.difficulty = difficulty;
    }

    private void generateSnakeAtCenter() {
        int x = MAX_INDEX_X / 2;
        int y = MAX_INDEX_Y / 2;
        snakeBody.add(new Point(x, y));
        occupiedPositions.add(snakeBody.getFirst());
        squaresToGrow += GROWTH;
    }

    private void generateApple() {

        int x;
        int y;
        boolean spaceIsOccupied;
        do {
            spaceIsOccupied = false;
            x = random.nextInt(MAX_INDEX_X);
            y = random.nextInt(MAX_INDEX_Y);
            for (Point point : occupiedPositions) {
                if (point.getX() == x && point.getY() == y) {
                    spaceIsOccupied = true;
                }
            }
        } while (spaceIsOccupied);
        apple.setLocation(x, y);
    }

    public void moveSnake() {

        int nextHeadX = (int) snakeBody.getFirst().getX();
        int nextHeadY = (int) snakeBody.getFirst().getY();

        switch (direction) {
            case UP:
                 nextHeadY -= 1;
                break;
            case DOWN:
                 nextHeadY += 1;
                break;
            case LEFT:
                 nextHeadX -= 1;
                break;
            case RIGHT:
                nextHeadX += 1;
                break;
            default:
                break;
        }

        if (snakeCollided(nextHeadX, nextHeadY)) {
            isGameOver = true;
            this.stopModel();
        }

        snakeBody.getLast().setLocation(nextHeadX, nextHeadY);
        if (ateApple()) {
            snakeBody.addFirst(new Point(nextHeadX, nextHeadY));
            occupiedPositions.add(snakeBody.getFirst());
            generateApple();
            applesEaten++;
            squaresToGrow += GROWTH - 1;
        } else if (squaresToGrow > 0) {
            snakeBody.addFirst(new Point(nextHeadX, nextHeadY));
            occupiedPositions.add(snakeBody.getFirst());
            squaresToGrow--;
        } else {
            snakeBody.addFirst(snakeBody.removeLast());
        }

        for (var e : this.snakeBody)
        {
            if (e.x >= MAX_INDEX_X) e.x -= MAX_INDEX_X;
            if (e.x < 0) e.x += MAX_INDEX_X;
            if (e.y >= MAX_INDEX_X) e.y -= MAX_INDEX_Y;
            if (e.y < 0) e.y += MAX_INDEX_Y;
        }
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    private boolean ateApple() {
        return snakeBody.getFirst().equals(apple);
    }

    /**
     * Checks for snake collision with self
     */
    private boolean snakeCollided(int nextHeadX, int nextHeadY) {
        return snakeBody.contains(new Point(nextHeadX, nextHeadY));
    }

    @Override
    public void run() {
        generateSnakeAtCenter();
        generateApple();

        int delay = 200;
        if (difficulty == 4) delay = 1000;
        if (difficulty == 1) delay = 140;
        if (difficulty == 2) delay = 70;
        if (difficulty == 3) delay = 50;

        this.timer = new Timer(delay, (e) -> {
            this.moveSnake();
            this.updater.update();
        });
        timer.start();
    }

    public void launchModel()
    {
        this.timer.start();
    }

    public void stopModel()
    {
        this.timer.stop();
    }

    public void setUpdater(Updater updater)
    {
        this.updater = updater;
    }
}
