package ru.nsu.g.ibaranov.view.gui;

import ru.nsu.g.ibaranov.model.Model;
import ru.nsu.g.ibaranov.utils.Direction;
import ru.nsu.g.ibaranov.utils.Updater;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Deque;


public class GamePanel extends JPanel implements Updater {

    /*private class IconManager {
        private final ImageIcon[] icons = new ImageIcon[TOTAL_ICONS];
        private final ImageIcon[] resizedIcons = new ImageIcon[TOTAL_ICONS];

        private static final int TOTAL_ICONS = 3;

        public static final int DRAW_VOID = 0;
        public static final int DRAW_APPLE = 1;
        public static final int DRAW_SNAKE = 2;


        IconManager() throws IOException {
            this.loadIcons();
        }

        private void loadIcons() throws IOException {
            for (int i = 0; i < TOTAL_ICONS; i++) {
                Image im = read(new File(String.format("src/Sprites/%d.png", i)));
                this.resizedIcons[i] = this.icons[i] = new ImageIcon(im);
            }
        }

        void resizeIcons(
                int width,
                int height) {
            for (int i = 0; i < TOTAL_ICONS; ++i) {
                this.resizedIcons[i] = new ImageIcon(
                        this.icons[i].getImage().getScaledInstance(
                                width,
                                height,
                                Image.SCALE_FAST
                        )
                );
            }
        }

        public ImageIcon getIcon(int cellValue) {
            return this.resizedIcons[cellValue];
        }
    }*/

    Model model;

    public GameWindow gameWindow;
    boolean isPause = false;
    boolean buttonPressed = false;
    private Direction direction = Direction.UP;
    // private IconManager iconManager = new IconManager();
    private Deque<Point> snakeBody;
    private Point apple;
    private int width, height;
    private Graphics2D g2d;


   // private JLabel[] cells; //= new JLabel[width*height];

    public GamePanel(Model model, GameWindow gm) {
        this.gameWindow = gm;
        this.setFocusable(true);
        this.model = model;
        this.model.setCurrentPlayerName(this.gameWindow.currentPlayerName);
        this.width = gm.getWidth();
        this.height = gm.getHeight();
        this.apple = model.getApple();
        this.setSnakeBody(model.getSnakeBody());

        //this.iconManager.resizeIcons(scale, scale);

        model.setUpdater(this);
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        paintApple();
        paintSnake();

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }

    @Override
    public Color getBackground() {
        return Color.black;
    }

    @Override
    public boolean isOpaque() {
        return true;
    }

    public void setSnakeBody(Deque<Point> snakeBody) {
        this.snakeBody = snakeBody;
    }

    /*public void initGrid() {
        this.setLayout(new GridLayout(this.width, this.height));

        this.cells = new JLabel[width * height];

        for (int i = 0; i < width * height; i++) {
            this.cells[i] = new JLabel();
            this.cells[i].setIcon(this.iconManager.getIcon(IconManager.DRAW_VOID));
            this.add(this.cells[i]);
        }

        this.addComponentListener(
                new ComponentAdapter() {

                    @Override
                    public void componentResized(ComponentEvent e) {
                        int width = gameWindow.getWidth() / gameWindow.getSIZE();
                        int height = gameWindow.getHeight() / gameWindow.getSIZE();
                        iconManager.resizeIcons(
                                width,
                                height
                        );
                        updateAll();
                    }
                });

    }*/

    /*public void updateAll()
    {
        for (JLabel cell : this.cells) {
            cell.setIcon(this.iconManager.getIcon(IconManager.DRAW_VOID));
        }
        for (var p : this.snakeBody)
        {
            this.cells[p.y * this.width + p.x].setIcon(this.iconManager.getIcon(IconManager.DRAW_SNAKE));
        }
        update();
    }*/

    public void paintSnake() {
        int xPos, yPos;
        for (Point position : snakeBody) {
            g2d.setColor(Color.green);
            int scaleX = ((int)(((double) this.gameWindow.getWidth())/this.gameWindow.getSIZE()));
            int scaleY = ((int)(((double) this.gameWindow.getHeight())/this.gameWindow.getSIZE()));
         //   scaleX *= 0.9;
           // scaleY *= 0.9;
            xPos = (int) position.getX() * (scaleX-1);
            yPos = (int) position.getY() * (scaleY-2);
            //g2d.drawRoundRect(xPos + 2, yPos + 2, scale - 4, scale - 4, 2, 2);
            g2d.drawRect(xPos , yPos ,  scaleX-2, scaleY-2);
        }
        //System.out.println(snakeColor);
        //r = 0;
        //g = 255;
        //b = 0;
    }

    /*public void paintGrid() {
        g2d.setStroke(new BasicStroke(0.25f));
        g2d.setColor(Color.gray);
        for (int i = 1; i < width / scale; i++) {
            g2d.drawLine(i * scale, 0, i * scale, height);
        }
        for (int i = 1; i < height / scale; i++) {
            g2d.drawLine(0, i * scale, width, i * scale);

        }
    }*/

    public void paintApple() {
        g2d.setStroke(new BasicStroke(1.5f));
        int xPos, yPos;
        g2d.setColor(Color.red);
        int scaleX = ((int)(((double) this.gameWindow.getWidth())/this.gameWindow.getSIZE()));
        int scaleY = ((int)(((double) this.gameWindow.getHeight())/this.gameWindow.getSIZE()));
        xPos = (int) this.apple.getX() * (scaleX-1);
        yPos = (int) this.apple.getY() * (scaleY-2);
        g2d.drawOval(xPos,yPos, scaleX-2, scaleY-2);
    }

    @Override
    public void update() {

        if (model.isGameOver) {
            model.setIsGameOver(false);
            this.gameWindow.setResizable(false);
            this.gameWindow.applesEated = model.getApplesEaten();
            this.gameWindow.showGameOverPanel(this.gameWindow.getContentPane(), this.model);
        }
/*
        var xTail = this.snakeBody.getLast().x;
        var yTail = this.snakeBody.getLast().y;
        //this.cells[yTail * this.width + xTail].setIcon(this.iconManager.getIcon(IconManager.DRAW_VOID));

        var xHead = this.snakeBody.getFirst().x;
        var yHead = this.snakeBody.getFirst().y;
        //this.cells[yHead * this.width + xHead].setIcon(this.iconManager.getIcon(IconManager.DRAW_SNAKE));

        var xApple = (int) this.apple.getX();
        var yApple = (int) this.apple.getY();
       // this.cells[yApple * this.width + xApple].setIcon(this.iconManager.getIcon(IconManager.DRAW_APPLE));

        if (yHead * this.width + xHead == yApple * this.width + xApple)
         //   this.cells[yApple * this.width + xApple].setIcon(this.iconManager.getIcon(IconManager.DRAW_SNAKE));
*/
        this.removeAll();
        this.repaint();
        this.gameWindow.nextDirrection = null;
    }

    public void directionInput(KeyEvent key) {

        if (gameWindow.nextDirrection == null) gameWindow.nextDirrection = Direction.UP;
        else return;

        switch (key.getKeyCode()) {
            case KeyEvent.VK_UP:
                if (direction != Direction.DOWN && !buttonPressed) {
                    direction = Direction.UP;
                }
                buttonPressed = true;
                break;
            case KeyEvent.VK_DOWN:
                if (direction != Direction.UP && !buttonPressed) {
                    direction = Direction.DOWN;
                }
                buttonPressed = true;
                break;
            case KeyEvent.VK_LEFT:
                if (direction != Direction.RIGHT && !buttonPressed) {
                    direction = Direction.LEFT;
                }
                buttonPressed = true;
                break;
            case KeyEvent.VK_RIGHT:
                if (direction != Direction.LEFT && !buttonPressed) {
                    direction = Direction.RIGHT;
                }
                buttonPressed = true;
                break;
            case KeyEvent.VK_P:
                //Pause
                this.isPause = !this.isPause;
                if (isPause) this.model.stopModel();
                else this.model.launchModel();
                this.gameWindow.nextDirrection = null;
                break;
            case KeyEvent.VK_ESCAPE:
                //Pause
                this.model.stopModel();
                this.model.setIsGameOver(false);
                this.gameWindow.showMenu(this.gameWindow.getContentPane());
                break;
            default:
                break;
        }

        if (direction != null) {
            model.setDirection(direction);
        }

        buttonPressed = false;
    }
}
