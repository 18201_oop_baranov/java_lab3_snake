package ru.nsu.g.ibaranov.view.gui;

import ru.nsu.g.ibaranov.utils.Entry;
import ru.nsu.g.ibaranov.utils.ScoreBoardManager;
import ru.nsu.g.ibaranov.view.gui.backgrounds.ScoreBackground;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.List;

public class ScorePanel extends JPanel {

    private GameWindow gameWindow;

    public ScorePanel(GameWindow gameWindow) throws IOException {
        this.gameWindow = gameWindow;
        ScoreBoardManager scoreBoardManager = new ScoreBoardManager();

        List<Entry> list = scoreBoardManager.loadData();

        JLayeredPane lp = new JLayeredPane();

        this.setLayout(new BorderLayout());

        var menuButton = new JButton();

        menuButton.setFocusPainted(false);
        menuButton.setFont(new Font(null, Font.BOLD, 40));
        menuButton.setText("Menu");
        menuButton.setBounds(170, 480, 240, 75);
        lp.add(menuButton, JLayeredPane.PALETTE_LAYER);

        menuButton.addActionListener(actionEvent -> this.gameWindow.showMenu(this.gameWindow.getContentPane()));

        ScoreBackground bc = new ScoreBackground();

        bc.setBounds(0, 0, 600, 600);
        lp.add(bc, JLayeredPane.DEFAULT_LAYER);

        EntryPanel[] entryes;// = new JLabel[8];
        entryes = new EntryPanel[8];
        for (int i=0 ; i<8; i++)
        {
            entryes[i] = new EntryPanel(list.get(i));
            if (i == 0) entryes[i].setBackground(Color.YELLOW);
            if (i == 1) entryes[i].setBackground(Color.magenta);
            if (i == 2) entryes[i].setBackground(Color.CYAN);
            if (i > 2) entryes[i].setBackground(Color.lightGray);

            entryes[i].setLocation(150, 154 + i*39);
            lp.add(entryes[i], JLayeredPane.PALETTE_LAYER);
        }

        this.add(lp);
    }
}
