package ru.nsu.g.ibaranov.view.gui;

import ru.nsu.g.ibaranov.utils.Entry;

import javax.swing.*;
import java.awt.*;

public class EntryPanel extends JPanel {

    public EntryPanel(Entry entry) {
        JLabel[] entryLabel = new JLabel[3];

        this.setLayout(new GridLayout(1,3));

        entryLabel[0] = new JLabel();
        entryLabel[0].setText(entry.getName());
        entryLabel[0].setFont(new Font(null, Font.BOLD, 18));

        entryLabel[1] = new JLabel();
        entryLabel[1].setText(entry.getDifficulty());
        entryLabel[1].setFont(new Font(null, Font.BOLD, 18));

        entryLabel[2] = new JLabel();
        entryLabel[2].setText(String.valueOf(entry.getValue()));
        entryLabel[2].setFont(new Font(null, Font.BOLD, 18));

        this.setSize(350,29);
        this.setBackground(Color.WHITE);

        for(int i=0; i<3; i++)
            this.add(entryLabel[i]);

        this.setVisible(true);
    }
}