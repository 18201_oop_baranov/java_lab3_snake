package ru.nsu.g.ibaranov.view.gui.backgrounds;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

import static javax.imageio.ImageIO.read;

public class ScoreBackground extends JPanel {
    Image back;
    public ScoreBackground(){
        try {
            back = read(new File("src/Sprites/d.png"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void paint(Graphics g) {
        Graphics2D graphic2d = (Graphics2D) g;
        graphic2d.drawImage(back,0,0,null);
    }
}
