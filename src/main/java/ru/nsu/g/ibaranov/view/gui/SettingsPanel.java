package ru.nsu.g.ibaranov.view.gui;

import ru.nsu.g.ibaranov.view.gui.backgrounds.settingsBackground;

import javax.swing.*;
import java.awt.*;

public class SettingsPanel extends JPanel {

    private GameWindow gameWindow;

    boolean isEasy = false;
    boolean isMedium = true;
    boolean isHard = false;

    public SettingsPanel(GameWindow gameWindow) {
        this.gameWindow = gameWindow;

        if (this.gameWindow.difficulty == 1) {
            isHard = false;
            isEasy = true;
            isMedium = false;
        }
        if (this.gameWindow.difficulty == 2) {
            isHard = false;
            isEasy = false;
            isMedium = true;
        }
        if (this.gameWindow.difficulty == 3) {
            isHard = true;
            isEasy = false;
            isMedium = false;
        }

        JLayeredPane lp = new JLayeredPane();

        this.setLayout(new BorderLayout());
        var menuButton = new JButton();

        menuButton.setFocusPainted(false);
        menuButton.setFont(new Font(null, Font.BOLD, 40));
        menuButton.setText("Menu");
        menuButton.setBounds(170, 420, 240, 75);
        lp.add(menuButton, JLayeredPane.PALETTE_LAYER);
        //this.add(lp);

        menuButton.addActionListener(actionEvent -> this.gameWindow.showMenu(this.gameWindow.getContentPane()));

        this.setLayout(new BorderLayout());
        var name = new JTextField(15);

        name.setFont(new Font(null, Font.BOLD, 15));
        name.setText(this.gameWindow.currentPlayerName);
        name.setBounds(220, 283, 120, 35);
        lp.add(name, JLayeredPane.PALETTE_LAYER);

        name.addActionListener(actionEvent -> {
            String nameString = name.getText().replaceAll(" ", "");
            if (nameString.length() > 7) nameString = nameString.substring(0,7);
            this.gameWindow.currentPlayerName = nameString;
        });

        var confirmButton = new JButton();
        confirmButton.setFont(new Font(null, Font.BOLD, 15));
        confirmButton.setBounds(350, 283, 80, 35);
        confirmButton.setText("Apply");
        confirmButton.addActionListener(actionEvent -> name.postActionEvent());
        lp.add(confirmButton, JLayeredPane.PALETTE_LAYER);

        var groupModes = new ButtonGroup();

        var mode1 = new JRadioButton("Easy", isEasy);
        mode1.setFont(new Font(null, Font.BOLD, 15));
        mode1.setBounds(220, 350, 60, 35);
        mode1.setFocusPainted(false);
        groupModes.add(mode1);

        var mode2 = new JRadioButton("Medium", isMedium);
        mode2.setFont(new Font(null, Font.BOLD, 15));
        mode2.setBounds(280, 350, 80, 35);
        mode2.setFocusPainted(false);
        groupModes.add(mode2);

        var mode3 = new JRadioButton("Hard", isHard);
        mode3.setFont(new Font(null, Font.BOLD, 15));
        mode3.setBounds(360, 350, 70, 35);
        mode3.setFocusPainted(false);
        groupModes.add(mode3);

        lp.add(mode1, JLayeredPane.PALETTE_LAYER);
        lp.add(mode2, JLayeredPane.PALETTE_LAYER);
        lp.add(mode3, JLayeredPane.PALETTE_LAYER);

        mode1.addActionListener(actionEvent -> this.gameWindow.difficulty = 1);

        mode2.addActionListener(actionEvent -> this.gameWindow.difficulty = 2);

        mode3.addActionListener(actionEvent -> this.gameWindow.difficulty = 3);



        settingsBackground bc = new settingsBackground();

        bc.setBounds(0, 0, 600, 600);
        lp.add(bc, JLayeredPane.DEFAULT_LAYER);

        this.add(lp);
    }
}
