package ru.nsu.g.ibaranov.view.cli;

import ru.nsu.g.ibaranov.model.Model;
import ru.nsu.g.ibaranov.utils.ScoreBoardManager;

import java.io.IOException;
import java.util.Scanner;

import static java.lang.System.exit;

public class MenuPanelCLI implements Runnable{

    private Model model;
    private Scanner scanner = new Scanner(System.in);

    public MenuPanelCLI()
    {

    }

    public MenuPanelCLI(Model model)
    {
        this.model = model;
    }

    private void printOptions() throws IOException {
        System.out.println("Enter\n" +
                "  \"start\" to start a new game\n" +
                "  \"scores\" to view high scores\n" +
                "  \"exit\" to exit");
        String in;
        while (scanner.hasNext()) {
            in = scanner.next();
            switch (in) {
                case "start":
                    GamePanelCLI gamePanelCLI = new GamePanelCLI();
                    gamePanelCLI.run();
                    break;
                case "scores":
                    ScoreBoardManager scoreBoardManager = new ScoreBoardManager(this.model);
                    scoreBoardManager.printEntryList();
                    System.out.println("\n");
                    this.printOptions();
                    break;
                case "exit":
                    exit(1);
            }
        }
    }

    @Override
    public void run() {
        try {
            this.printOptions();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
