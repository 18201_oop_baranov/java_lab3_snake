package ru.nsu.g.ibaranov.view.gui;

import ru.nsu.g.ibaranov.model.Model;
import ru.nsu.g.ibaranov.utils.ScoreBoardManager;
import ru.nsu.g.ibaranov.view.gui.backgrounds.gmBackground;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class GameOverPanel extends JPanel {

    private GameWindow gameWindow;

    public GameOverPanel(GameWindow gameWindow, Model model) {
        this.gameWindow = gameWindow;

        ScoreBoardManager scoreBoardManager = new ScoreBoardManager(model);

        JLayeredPane lp = new JLayeredPane();

        this.setLayout(new BorderLayout());

        var nameLabel = new JLabel();
        nameLabel.setText("Name: " + this.gameWindow.currentPlayerName);
        nameLabel.setFont(new Font(null, Font.BOLD, 40));
        nameLabel.setForeground(Color.white);
        nameLabel.setBounds(170, 290, 340, 75);
        lp.add(nameLabel, JLayeredPane.PALETTE_LAYER);

        var applesLabel = new JLabel();
        applesLabel.setText("Score: " + this.gameWindow.applesEated);
        applesLabel.setFont(new Font(null, Font.BOLD, 40));
        applesLabel.setForeground(Color.white);
        applesLabel.setBounds(210, 340, 340, 75);
        lp.add(applesLabel, JLayeredPane.PALETTE_LAYER);

        var menuButton = new JButton();

        menuButton.setFocusPainted(false);
        menuButton.setFont(new Font(null, Font.BOLD, 40));
        menuButton.setText("Menu");
        menuButton.setBounds(170, 420, 240, 75);
        lp.add(menuButton, JLayeredPane.PALETTE_LAYER);

        menuButton.addActionListener(actionEvent -> {
            try {
                scoreBoardManager.saveData();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.gameWindow.showMenu(this.gameWindow.getContentPane());
        });

        gmBackground bc = new gmBackground();

        bc.setBounds(0, 0, 600, 600);
        lp.add(bc, JLayeredPane.DEFAULT_LAYER);

        this.add(lp);
    }

}
