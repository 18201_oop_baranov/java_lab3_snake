package ru.nsu.g.ibaranov.view.gui;

import ru.nsu.g.ibaranov.model.Model;
import ru.nsu.g.ibaranov.utils.Direction;
import ru.nsu.g.ibaranov.view.gui.backgrounds.Background;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import static javax.imageio.ImageIO.read;

public class GameWindow extends JFrame implements Runnable, KeyListener {

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        gamePanel.directionInput(keyEvent);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }

    private int WIDTH = 600;
    private int HEIGHT = 600;
    private int SIZE = 20;
    private GamePanel gamePanel;
    public String currentPlayerName = "Player";
    public int difficulty = 2;
    public int applesEated = 0;
    public Direction nextDirrection;

    public int getSIZE() {
        return SIZE;
    }

    public GameWindow() throws IOException {
        super("Snake");
        this.setIconImage(getIcon());
        this.setSize(WIDTH, HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        addKeyListener(this);
        setFocusable(true);

        this.showMenu(this.getContentPane());
    }

    public Image getIcon() throws IOException {
       return read(new File("src/Sprites/icon.png"));
    }

    public void showMenu(Container pane)
    {
        pane.removeAll();
        pane.add(new MenuPanel());
        this.revalidate();
    }

    public void showScore(Container pane) throws IOException {
        pane.removeAll();
        pane.add(new ScorePanel(this));
        this.revalidate();
    }

    public void showGameOverPanel(Container pane, Model model)
    {
        pane.removeAll();
        this.setSize(WIDTH, HEIGHT);
        pane.add(new GameOverPanel(this, model));
        this.revalidate();
    }

    public void showSettingsPanel(Container pane)
    {
        pane.removeAll();
        pane.add(new SettingsPanel(this));
        this.revalidate();
    }


    private void showGame(Container pane, Model model) {
        pane.removeAll();
        pane.add(this.gamePanel = new GamePanel(model, this));
        this.revalidate();
    }

    @Override
    public void run() {
        SwingUtilities.invokeLater(()->this.setVisible(true));
    }

    public class MenuPanel extends JPanel {

        private int MAX_X = GameWindow.this.SIZE;
        private int MAX_Y = GameWindow.this.SIZE;

        public MenuPanel()
        {
            JLayeredPane lp = new JLayeredPane();

            this.setLayout(new BorderLayout());
            var startButton = new JButton();

            startButton.addActionListener(actionEvent -> {
               // Model snake = new Model(GameWindow.this.difficulty, MAX_X,MAX_Y);
                Model snake = new Model(GameWindow.this.difficulty, MAX_X, MAX_Y);
                    GameWindow.this.setResizable(true);
                    //GameWindow.this.showGame(GameWindow.this.getContentPane(), snake, 600/GameWindow.this.SIZE);
                    GameWindow.this.showGame(GameWindow.this.getContentPane(), snake);
                    snake.run();
            });

            startButton.setFocusPainted(false);
            startButton.setFont(new Font(null, Font.BOLD, 25));
            startButton.setText("New game");
            startButton.setBounds(170, 220, 240, 75);
            lp.add(startButton, JLayeredPane.PALETTE_LAYER);

            var settingsButton = new JButton();
            settingsButton.setFocusPainted(false);
            settingsButton.setFont(new Font(null, Font.BOLD, 25));
            settingsButton.setText("Settings");
            settingsButton.setBounds(170, 300, 240, 75);
            lp.add(settingsButton, JLayeredPane.PALETTE_LAYER);

            settingsButton.addActionListener(actionEvent -> GameWindow.this.showSettingsPanel(GameWindow.this.getContentPane()));

            var scoreButton = new JButton();
            scoreButton.setFocusPainted(false);
            scoreButton.setFont(new Font(null, Font.BOLD, 25));
            scoreButton.setText("Score");
            scoreButton.setBounds(170, 380, 240, 75);
            lp.add(scoreButton, JLayeredPane.PALETTE_LAYER);

            scoreButton.addActionListener(actionEvent -> {
                try {
                    GameWindow.this.showScore(GameWindow.this.getContentPane());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            Background bc = new Background();

            bc.setBounds(0,0,600,600);
            lp.add(bc, JLayeredPane.DEFAULT_LAYER);

            JLabel me = new JLabel();
            me.setText("NSU Baranov I.N. 18201");
            me.setFont(new Font(null, Font.PLAIN, 10));
            me.setBounds(240, 500, 180, 40);
            me.setForeground(Color.ORANGE);
            me.setVisible(true);
            lp.add(me, JLayeredPane.POPUP_LAYER);

            this.add(lp);
        }
    }
}
