package ru.nsu.g.ibaranov.view.cli;

import ru.nsu.g.ibaranov.model.Model;
import ru.nsu.g.ibaranov.utils.Direction;
import ru.nsu.g.ibaranov.utils.ScoreBoardManager;
import ru.nsu.g.ibaranov.utils.Updater;

import java.awt.*;
import java.io.IOException;
import java.util.Deque;
import java.util.Scanner;

public class GamePanelCLI implements Updater, Runnable {

    private int MAX_X = 15;
    private int MAX_Y = 15;
    private int difficulty = 4;
    public char[] field = new char[MAX_Y*MAX_X];
    private Model model = new Model(difficulty, MAX_X,MAX_Y);
    private Direction nextDirrection;
    private Direction direction = Direction.UP;
    private boolean buttonPressed;
    private boolean isGameOver;
    private Deque<Point> snakeBody;
    Scanner scanner;
    private Point apple;

    public GamePanelCLI() {
        initField();
        this.isGameOver = false;
        this.scanner = new Scanner(System.in);
        this.snakeBody = this.model.getSnakeBody();
        this.apple = this.model.getApple();
        this.model.setUpdater(this);
    }

    private void initField()
    {
        for (int i=0; i<MAX_X*MAX_Y; i++) this.field[i] = ' ';
    }

    private void printField()
    {
        int k = 0;
        int i;
        for (i=0; i<(MAX_X+2)*(MAX_Y+2) + MAX_Y + 2; i++) {
            if ((i+1) % (MAX_Y+3) == 0) {
                System.out.print('\n');
                k++;
            } else {
                if (i - MAX_Y < 3 || (i) % (MAX_Y+3) == 0  || (i+2) % (MAX_Y+3) == 0 || (i - ((MAX_X+2)*(MAX_Y+2) - 2)) >= 0)
                {
                    System.out.print('#');
                    k++;
                }
                else {
                    System.out.print(this.field[i-k]);
                }
            }
        }
    }

    @Override
    public void run() {

        System.out.println("Type your name: ");
        this.model.setCurrentPlayerName(this.scanner.nextLine());

        this.model.run();

        String in;
        while (scanner.hasNext()) {

            if (this.nextDirrection == null) this.nextDirrection = Direction.UP;
            else break;

            in = scanner.next();
            switch (in) {
                case "a":
                    if (direction != Direction.RIGHT && !buttonPressed) {
                        direction = Direction.LEFT;
                    }
                    buttonPressed = true;
                    break;
                case "d":
                    if (direction != Direction.LEFT && !buttonPressed) {
                        direction = Direction.RIGHT;
                    }
                    break;
                case "w":
                    if (direction != Direction.DOWN && !buttonPressed) {
                        direction = Direction.UP;
                    }
                    buttonPressed = true;
                    break;
                case "s":
                    if (direction != Direction.UP && !buttonPressed) {
                        direction = Direction.DOWN;
                    }
                    break;
                case "e":
                    model.stopModel();
                    isGameOver = true;
                    MenuPanelCLI menu = new MenuPanelCLI(this.model);
                    menu.run();
                    break;
            }

            if (direction != null) {
                model.setDirection(direction);
            }

            buttonPressed = false;
        }
    }

    @Override
    public void update() {
        if (model.isGameOver) {
            model.setIsGameOver(false);
            ScoreBoardManager scoreBoardManager = new ScoreBoardManager(model);
            try {
                scoreBoardManager.saveData();
            }
            catch (IOException ignored)
            {

            }
            MenuPanelCLI menu = new MenuPanelCLI(this.model);
            menu.run();
            return;
        }

        var xTail = this.snakeBody.getLast().x;
        var yTail = this.snakeBody.getLast().y;
        this.field[yTail * this.MAX_X + xTail] = ' ';

        var xHead = this.snakeBody.getFirst().x;
        var yHead = this.snakeBody.getFirst().y;
        this.field[yHead * this.MAX_X + xHead] = '*';

        var xApple = (int) this.apple.getX();
        var yApple = (int) this.apple.getY();
        this.field[yApple * this.MAX_X + xApple] = '@';

        if (yHead * this.MAX_X + xHead == yApple * this.MAX_X + xApple)
            this.field[yApple * this.MAX_X + xApple] = '*';

        this.nextDirrection = null;
        this.printField();
    }
}
