package ru.nsu.g.ibaranov;

import ru.nsu.g.ibaranov.view.cli.MenuPanelCLI;
import ru.nsu.g.ibaranov.view.gui.GameWindow;

import java.io.IOException;

public class MainCLI {
    public static void main(String[] args) throws IOException {
        MenuPanelCLI menuPanelCLI = new MenuPanelCLI();
        menuPanelCLI.run();
    }
}
