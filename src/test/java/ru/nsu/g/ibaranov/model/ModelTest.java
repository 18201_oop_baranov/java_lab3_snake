package ru.nsu.g.ibaranov.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModelTest {
    @Test
    void testConstructor()
    {
        Model model = new Model(3, 20,20);
        assertEquals(model.getDifficulty(), 3);
        assertEquals(model.getMAX_INDEX_X(), 20);
        assertEquals(model.getMAX_INDEX_Y(), 20);
    }

    @Test
    void testSetters()
    {
        Model model = new Model(3,20,20);
        model.setCurrentPlayerName("Player");
        assertEquals(model.getCurrentPlayerName(), "Player");

        model.setIsGameOver(true);
        assertTrue(model.isGameOver);
    }
}